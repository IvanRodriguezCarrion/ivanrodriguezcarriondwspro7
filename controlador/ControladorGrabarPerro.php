<?php session_start();?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Gestión de Personas</title>
        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.5.0/pure-min.css">
	<meta charset="UTF-8">
    </head>
    <body >

        <div class="pure-g">
            <div class="pure-u-1-12">
		<?php
		error_reporting(E_ALL);
		ini_set('display_errors', '1');
		include_once  __DIR__.'/Funciones.php';	
		
		$id = recoge('id');
		$nombre = recoge('nombre');
		$raza = recoge('raza');
		$nChip = recoge('numChip');
		$propietario = recoge('propietario');
		
		$objetoPropietario = new Persona ($propietario, null, null, null, null);

		if ($id != "" && $nombre != "" && $raza != "" && $nChip != "" && $objetoPropietario->__GET('id') != "") {
		    $perro = new Perro($id, $nombre, $raza, $nChip, $objetoPropietario->__GET('id'));
		    $grabarArchivo = comprobarModelo();
		    $grabarArchivo->createPerro($perro);
		    header("Location: ../vista/VistaPerro.php");
		    echo "Perro grabado con éxito.";
		    echo "<a href = ../vista/VistaPerro.php> Volver al menú de perros</a>";
		} else {
		    echo "Alguno de los campos está vacío.";
		    echo "<a href = ../vista/VistaPerro.php> Volver al menú de perros</a>";
		}
		?>
                </table>     

            </div>
        </div>

    </body>
</html>
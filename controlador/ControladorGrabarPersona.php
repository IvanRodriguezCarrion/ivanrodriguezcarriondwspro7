<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Gestión de Personas</title>
        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.5.0/pure-min.css">
	<meta charset="UTF-8">
    </head>
    <body >

        <div class="pure-g">
            <div class="pure-u-1-12">
		<?php
		error_reporting(E_ALL);
		ini_set('display_errors', '1');

		include_once( __DIR__.'/Funciones.php');
		//include_once( __DIR__.'/../modelo/Persona.php');

		$id = recoge('id');
		$nombre = recoge('nombre');
		$apellido = recoge('apellido');

		if ($id != "" && $nombre != "" && $apellido != "" ) {
		    $persona = new Persona($id, $nombre, $apellido);
		    $grabarArchivo = comprobarModelo();
		    $grabarArchivo->createPersona($persona);
		     header("Location: ../vista/VistaPersona.php");
		    echo "Persona grabada con éxito.";
		    echo "<a href = ../vista/VistaPersona.php> Volver al menú de personas</a>";
		} else {
		    echo "Alguno de los campos está vacío.";
		    echo "<a href = ../vista/VistaPersona.php> Volver al menú de personas</a>";
		}
		?>
                </table>     

            </div>
        </div>

    </body>
</html>

<?php
if(!isset($_SESSION)) {
    session_start();
}

function deslogear() {
    unset($_SESSION['autenticar']);
    header("Location: ../index.php");
}

deslogear();
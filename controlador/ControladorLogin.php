<?php

include_once( __DIR__.'/Funciones.php');

if(!isset($_SESSION)) {
    session_start();
}

$usuario = recoge("nombre");
$pass = recoge("pass");

if ($usuario === "admin" && $pass === "admin") {
    $_SESSION['autenticar'] = "autenticado";
    header("Location: ../vista/VistaModelo.php");
} else {
    header("Location: ../index.php");
}
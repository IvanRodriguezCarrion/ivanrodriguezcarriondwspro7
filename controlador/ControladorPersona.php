<?php

function rellenarCBPersonas() {
    $modelo = comprobarModelo();
    foreach ($modelo->readPersona() as $r):
	?>
	<option value ="<?php echo $r->__GET('id'); ?>"><?php echo $r->__GET('nombre'); ?></option>
	<?php
    endforeach;
}

function rellenarTablaPersona() {
    $modelo = comprobarModelo();
    foreach ($modelo->readPersona() as $r):
	?>
	<tr>
	    <td><?php echo $r->__GET('id'); ?></td>
	    <td><?php echo $r->__GET('nombre'); ?></td>
	    <td><?php echo $r->__GET('apellidos'); ?></td>
	</tr>
	<?php
    endforeach;
}

function calcularIDPersonas() {
    $modelo = comprobarModelo();
    $personas = $modelo->idPersona();
    echo $personas;
}

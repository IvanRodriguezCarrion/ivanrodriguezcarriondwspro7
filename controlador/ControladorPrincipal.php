<?php session_start();?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Gestión de Personas</title>
	<meta charset="UTF-8">
    </head>
    <body >
	<div>
	    <?php
	    error_reporting(E_ALL);
	    ini_set('display_errors', '1');
	    include_once( __DIR__.'/Funciones.php');
	    include_once( __DIR__.'/../modelo/ModeloMysql.php');
	    include_once( __DIR__.'/../modelo/ModeloFicheros.php');
	   
	    $_SESSION['modelo'] = recoge('modelo');
	    echo "Has escogido el modelo " . $_SESSION['modelo'] . " para trabajar." ;
	    
	    if (recoge('instalar') === "activado"){
	    echo "<p>La base de datos comenzará a instalarse:.</p>";
	    $modelo = comprobarModelo();
	    $modelo->instalarBD();
	    }
	    header("Location: ../vista/VistaPrincipal.php");
	    echo "<a href = ../vista/VistaPrincipal.php>Haga click aquí para continuar.</a> <br/>";
	    ?>
	</div> 

    </body>
<?php

include_once( __DIR__.'/Persona.php');
include_once( __DIR__.'/Perro.php');
include_once( __DIR__.'/Modelo.php');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ModeloFicheros
 *
 * @author BlackBana
 */
class ModeloFicheros implements Modelo {

    private $fpersonas = "../media/database/personas.csv";
    private $fperros = "../media/database/perros.csv";
    
    public function instalarBD() {
	if (!file_exists("../media/database/personas.csv")) {
	    $archivo = fopen("../media/database/personas.csv", "w");
	    fclose($archivo);
	}
	if (!file_exists("../media/database/perros.csv")) {
	    $archivo = fopen("../media/database/perros.csv", "w");
	    fclose($archivo);
	}
    }
    

    public function createPersona($persona) {
	$f = fopen($this->fpersonas, "a");
	$linea = $persona->__GET('id') . ";"
		. $persona->__GET('nombre') . ";"
		. $persona->__GET('apellidos') . "\r\n";
	fwrite($f, $linea);
	fclose($f);
    }

    public function readPersona() {
	$personas = array();

	if ($archivo = fopen($this->fpersonas, "r")) {
	    $token = fgetcsv($archivo, 0, ";"); // fgetcsv se comporta como fgets pero para ficheros csv
	    while ($token) {
		$persona = new Persona($token[0], $token[1], $token[2]);
		array_push($personas, $persona);
		$token = fgetcsv($archivo, 0, ";");
	    }
	    fclose($archivo);
	} else {
	    echo "Esto es un error al abrir";
	}

	return $personas;
    }

    public function createPerro($perro) {
	$f = fopen($this->fperros, "a");
	$linea = $perro->__GET('id') . ";"
		. $perro->__GET('nombre') . ";"
		. $perro->__GET('raza') . ";"
		. $perro->__GET('numChip') . ";"
		. $perro->__GET('propietario') . "\r\n";
	fwrite($f, $linea);
	fclose($f);
    }

    public function readPerro() {
	$perros = array();

	if ($archivo = fopen($this->fperros, "r")) {
	    $token = fgetcsv($archivo, 0, ";"); // fgetcsv se comporta como fgets pero para ficheros csv
	    while ($token) {
		$persona = new Persona($token[4],null, null);
		$perro = new Perro($token[0], $token[1], $token[2], $token[3], $persona);
		array_push($perros, $perro);
		//agregar funcion recuperar nombre duenyo
		$token = fgetcsv($archivo, 0, ";");
	    }
	    fclose($archivo);
	} else {
	    //errorres
	}

	return $perros;
    }

    public function idPersona() {
	$modelo = new ModeloFicheros();
	$personas = $modelo->readPersona();
	$ultPersona = end($personas);
	$ultID = $ultPersona->__GET('id');
	$ultID++;
	echo $ultID;
    }

    public function idPerro() {
	$modelo = new ModeloFicheros();
	$perros = $modelo->readPerro();
	$ultPerro = end($perros);
	$ultID = $ultPerro->__GET('id');
	$ultID++;
	echo $ultID;
    }

    public function desinstalarBD() {
	
    }

}

<?php
require_once __DIR__."/Modelo.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ModeloMysql
 *
 * @author BlackBana
 */
class ModeloMysql implements Modelo {

    public function conectar() {
	$pdo = null;
	try {
	    $pdo = new PDO("mysql:host=localhost;dbname=" . Configuracion::$bdnombre, Configuracion::$bdusuario, Configuracion::$bdclave);
	    $pdo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, TRUE);
	} catch (PDOException $e) {
	    echo "<p>Error: No puede conectarse con la base de datos.</p>\n";
	    echo "<p>Error: " . $e->getMessage() . "</p>\n";
	}
	return $pdo;
    }

    public function desconectar() {
	return null;
    }

    public function instalarBD() {

	try {
	    $pdo = new PDO("mysql:host=localhost", Configuracion::$bdusuario, Configuracion::$bdclave);
	    $pdo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, TRUE);
	} catch (PDOException $e) {
	    echo "<p>Error: No puede conectarse con la base de datos.</p>\n";
	    echo "<p>Error: " . $e->getMessage() . "</p>\n";
	}

	$consulta = "CREATE DATABASE " . Configuracion::$bdnombre;

	$pdo->query($consulta);

	$pdo = $this->desconectar();

	$pdo = $this->conectar();

	$consulta = "CREATE TABLE personas ("
		. "id INT AUTO_INCREMENT,"
		. "nombre VARCHAR(25),"
		. "apellidos VARCHAR(35),"
		. "PRIMARY KEY (id));"
		. "CREATE TABLE perros ("
		. "id INT AUTO_INCREMENT,"
		. "nombre VARCHAR(25),"
		. "raza VARCHAR(25),"
		. "nChip INT,"
		. "propietario INT,"
		. "PRIMARY KEY (id),"
		. "FOREIGN KEY (propietario) REFERENCES personas(id));";
	$pdo->query($consulta);

	$consulta = "INSERT INTO personas (nombre, apellidos) VALUES ('Pablo', 'Perez');"
		. "INSERT INTO personas (nombre, apellidos) VALUES ('Pepe', 'Perez');"
		. "INSERT INTO perros (nombre, raza, nChip, propietario) VALUES ('Chacho', 'Mestizo', 3252, 1);"
		. "INSERT INTO perros (nombre, raza, nChip, propietario) VALUES ('Chicho', 'PPP', 25242, 1);";
	$pdo->query($consulta);

	$pdo = $this->desconectar();
    }

    public function createPersona($persona) {
	$pdo = $this->conectar();
	$consulta = "INSERT INTO personas (nombre, apellidos) VALUES (:nombre, :apellidos);";
	$pdo->prepare($consulta)->execute(array(":nombre" => $persona->__GET('nombre'), ":apellidos" => $persona->__GET('apellidos')));
	$pdo = $this->desconectar();
    }

    public function readPersona() {
	$personas = array();
	$pdo = $this->conectar();
	$consulta = "SELECT * FROM personas";
	$stm = $pdo->prepare($consulta);
	$stm->execute();
	foreach($stm->fetchAll(PDO::FETCH_OBJ) as $row){
	    $persona = new Persona(0, "","");
	    $persona->__SET('id', $row->id);
            $persona->__SET('nombre', $row->nombre);
            $persona->__SET('apellidos', $row->apellidos); 
	    
	    $personas[] = $persona;
	}
	$pdo = $this->desconectar();
	return $personas;
    }

    public function createPerro($perro) {
	$pdo = $this->conectar();
	$consulta = "INSERT INTO perros (nombre, raza, nChip, propietario) VALUES (:nombre, :raza, :nChip, :propietario);";
	$pdo->prepare($consulta)->execute(array(":nombre"      => $perro->__GET('nombre'), 
						":raza"        => $perro->__GET('raza'), 
						":nChip"       => $perro->__GET('numChip'), 
						":propietario" => $perro->__GET('propietario')
							  ));
	$pdo = $this->desconectar();
    }

    public function readPerro() {
	$perritos = array();
	$pdo = $this->conectar();
	$consulta = "SELECT * FROM perros";
	$stm = $pdo->prepare($consulta);
	$stm->execute();
	foreach($stm->fetchAll(PDO::FETCH_OBJ) as $row){
	    $perro = new Perro(0, "","","",null);
	    $persona = new Persona(0, "","");
	    
	    $persona->__SET('id', $row->propietario);
	    
	    $perro->__SET('id', $row->id);
	    $perro->__SET('nombre', $row->nombre);
	    $perro->__SET('raza', $row->raza);
	    $perro->__SET('numChip', $row->nChip);
	    $perro->__SET('propietario', $persona);
	               
	    $perritos[] = $perro;
	}
	$pdo = $this->desconectar();
	return $perritos;
    }

    public function idPersona() {
	$pdo = $this->conectar();  
	$id = $pdo->query("SELECT MAX(id) AS id FROM personas")->fetch(PDO::FETCH_NUM);
	$pdo = $this->desconectar();
	return $id[0] + 1;
    }

    public function idPerro() {
	$pdo = $this->conectar();  
	$id = $pdo->query("SELECT MAX(id) AS id FROM perros")->fetch(PDO::FETCH_NUM);
	$pdo = $this->desconectar();
	return $id[0] + 1;
    }

    public function desinstalarBD() {
	
    }

}

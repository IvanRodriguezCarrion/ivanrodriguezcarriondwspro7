<?php
include_once __DIR__ . '/../controlador/Funciones.php';
acceso();
?>
<!DOCTYPE html>
<html>
    <head>
	<title>Bienvenido a gestión general</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
	<?php
	cabecera();
	?>
	<h1> Gestión de apadrinamientos Ringo S.L</h1>
	<form action="../controlador/ControladorPrincipal.php" method="post">
	    <p>¿Sobre qué tipo de base de datos quiere trabajar?</p>
	    <div>
		<input type="radio" name="modelo" value="ficheros" checked = "checked" /> Ficheros<br />
		<input type="radio" name="modelo" value="mysql" /> MySql<br />
	    </div>
	    <div>
		<p><input type="checkbox" name="instalar" value="activado"> ¿Instalar la base de datos?</p>
	    </div>
	    <button type="submit" name="fichero">Escoger</button>
	</form>
	<div id="documentacion">
	    <p><a href="../media/documentacion/documentacion.pdf">Documentación en PDF.</a></p>
	</div>
	<?php
	botonDesloguear();
	pie();
	?>
    </body>
</html>

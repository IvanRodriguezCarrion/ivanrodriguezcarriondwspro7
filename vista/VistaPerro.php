<?php
include_once __DIR__ . '/../controlador/Funciones.php';
include_once(__DIR__.'/../controlador/ControladorPersona.php');
include_once __DIR__.'/../controlador/ControladorPerro.php';
acceso();
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Gestión de Perros</title>
	<meta charset="UTF-8">
    </head>
    <body >
	<?php
	cabecera();
	if (Configuracion::$modelo !== "") {
	    echo "Has escogido el modelo " . Configuracion::$modelo . " para trabajar." ;
	}
	?>
	<h1> Alta de nuevos perros</h1>
        <div>
            <div>

                <form action="../controlador/ControladorGrabarPerro.php" method="post" >
                    <table >
			<tr>
                            <th >Identificador</th>
                            <td><input type="text" name="id" value="<?php calcularIDPerros() ?>" readonly="readonly" /></td>
                        </tr>
			<tr>
                            <th >Nombre</th>
                            <td><input type="text" name="nombre" value="" placeholder="nombre del perro" /></td>
                        </tr>
                        <tr>
                            <th >Raza</th>
                            <td><input type="text" name="raza" value=""  /></td>
                        </tr>
			<tr>
                            <th >Número Chip</th>
                            <td><input type="text" name="numChip" value=""  /></td>
                        </tr>
                        <tr>
                            <th >Proprietario</th>
                            <td>
                                <select name="propietario">
				    <option value ="0"> Escoja un propietario </option>
				    <?php
				    rellenarCBPersonas();
				    ?>
                                </select>
                            </td>
                        </tr>
			<tr>
                            <td colspan="2">
                                <button type="submit">Guardar</button>
                            </td>
                        </tr>
                    </table>
                </form>

                <table>
                    <thead>
                        <tr>
			    <th>ID</th>
                            <th >Nombre</th>
                            <th >Raza</th>
                            <th >Número de chip</th>
                            <th >Propietario</th>
                        </tr>
                    </thead>

		    <!--Aqui podemos encapsular esta función en un archivo distinto y meterla como si fuese un mixin de sass-->
		    <?php
		    rellenarTablaPerro();
		    ?>
                </table>     

            </div>
        </div>
	<?php
	botonDesloguear();
	pie();
	inicio();
	?>
    </body>
</html>
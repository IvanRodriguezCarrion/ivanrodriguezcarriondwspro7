<?php
include_once __DIR__ . '/../controlador/Funciones.php';
include_once(__DIR__.'/../controlador/ControladorPersona.php');
acceso();
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Gestión de Personas</title>
	<meta charset="UTF-8">
    </head>
    <body >
	<?php
	cabecera();
	?>
	<h1> Alta de nuevas personas</h1>
        <div>
            <div>

                <form action="../controlador/ControladorGrabarPersona.php" method="post" >
                    <table >
			<tr>
                            <th >Identificador</th>
                            <td><input type="text" name="id" value="<?php calcularIDPersonas() ?>" readonly="readonly" s/></td>
                        </tr>
			<tr>
                            <th >Nombre</th>
                            <td><input type="text" name="nombre" value=""  /></td>
                        </tr>
                        <tr>
                            <th >Apellido</th>
                            <td><input type="text" name="apellido" value=""  /></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <button type="submit">Guardar</button>
                            </td>
                        </tr>
                    </table>
                </form>

                <table>
                    <thead>
                        <tr>
			    <th>ID</th>
                            <th >Nombre</th>
                            <th >Apellido</th>
                        </tr>
                    </thead>

		    <!--Aqui podemos encapsular esta función en un archivo distinto y meterla como si fuese un mixin de sass-->
		    <?php rellenarTablaPersona(); ?>
                </table>     

            </div>
        </div>
	<?php
	botonDesloguear();
	pie();
	inicio();
	?>
    </body>
</html>
<?php
include_once __DIR__ . '/../controlador/Funciones.php';
acceso();
?>
<!DOCTYPE html>
<html>
    <head>
	<title>Bienvenido a gestión general</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<style>
	    #menu {
		display: flex;
		flex-direction: row;
		flex-wrap: wrap;
		justify-content: center;
		align-items: center;
	    }
	</style>
    </head>
    <body>
	<?php
	cabecera();
	?>
	<h1> Gestión de apadrinamientos Ringo S.L</h1>
	<div id="menu">
	    <a href="VistaPersona.php"><img src ="../media/images/persona.jpg"/></a>
	    <a href="VistaPerro.php"><img src ="../media/images/perro.jpg"/></a>
	</div>

	<div id="documentacion">
	    <a href="media/documentacion/documentacion.pdf">Documentación en PDF</a>
	</div>
	
	<?php
	botonDesloguear();
	pie();
	?>
    </body>
</html>
